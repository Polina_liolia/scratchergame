const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const CopyPlugin = require('copy-webpack-plugin');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: ['babel-polyfill', './js/script.js', './css/style.css'],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
    },
    // watch: true,
    devtool: 'inline-source-map',
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.(scss|css)$/,
                resolve: {
                    extensions: [".scss", ".css"],
                },
                use: [
                    'style-loader',
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader?sourceMap',
                ]
            },
            {
                test: /\.json$/,
                loader: 'json'
            },
            {
                test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader']
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.css', '.scss'],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: `[name].css`
        }),
        new ExtractTextPlugin({
            filename: 'style.css'
        }),
        new CopyPlugin([{
                from: './Assets',
                to: './',
                flatten: true,
            },
            {
                from: './index.html',
                to: './'
            },
            // {
            //     from: './favicon.ico',
            //     to: './'
            // },
            {
                from: './fonts',
                to: './'
            },
            {
                from: './libs',
                to: './',
                flatten: true
            }
        ]),
    ],
    optimization: {
       
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    ecma: 8,
                    warnings: false,
                    compress: {
                        drop_console: true
                    },
                    output: {
                        comments: false,
                        beautify: false,
                    },
                    toplevel: false,
                    nameCache: null,
                    ie8: true,
                    keep_classnames: undefined,
                    keep_fnames: false,
                    safari10: true,
                },
                extractComments: false,
            })
        ]
    }

};