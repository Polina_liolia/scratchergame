import Game from "./Game";

export default class MainScene {
    constructor(game) {
        this.game = game;
        this.addResourcesToLoader();
        this.currentGamedData = null;
        this.tickerCounter = 0;
        this.cards = [];
        this.character = null;
        this.charState = "idle";
        this.cardsScratched = 0;
        this.gameStarted = false;
        this.isNewGame = true;
    }

    onResoursesLoaded() {
        this.buildScene();
        this.showPrePlayScreen();
    }


    addResourcesToLoader() {
        //adding character resource to loader to build preload animation:
        PIXI.loader.add('character', './red.json').load((loader, resources) => {
            let w = 1097;
            let h = 1920;
            let animation = new PIXI.spine.Spine(resources.character.spineData);
            animation.name = 'preloader';
            animation.width -= 200;
            animation.height -= 200;
            animation.x += (w - animation.width + 152) / 2;
            animation.y += (h - animation.height + 200) / 2;
            animation.skeleton.setToSetupPose();
            animation.update(0);
            animation.autoUpdate = true;

            // add the animation to the scene and render...
            this.game.app.stage.addChild(animation);

            if (animation.state.hasAnimation('red_loading_screen_animation_loop')) {
                animation.state.setAnimation(0, 'red_loading_screen_animation_loop', true);
            }

            //adding all scene resources we need to loader:
            loader.add('magic_forest', './magic_forest_bg.jpg')
                .add('bonfire', './magic_forest_bonfire.png')
                .add('bonfire_small', './magic_forest_bonfire_small.png')
                .add('bow', './magic_forest_bow.png')
                .add('bow_small', './magic_forest_bow_small.png')
                .add('button', './magic_forest_button.png')
                .add('coin_icon_big', './magic_forest_coin_icon_big.png')
                .add('coin_icon_small', './magic_forest_coin_icon_small.png')
                .add('dollar_icon', './magic_forest_dollar_icon.png')
                .add('frame', './magic_forest_frame.png')
                .add('frame1', './magic_forest_frame1.png')
                .add('frame2', './magic_forest_frame2.png')
                .add('frame3', './magic_forest_frame3.png')
                .add('frame_for_text', './magic_forest_frame_for_text.png')
                .add('gift_icon', './magic_forest_gift_icon.png')
                .add('leaf', './magic_forest_leaf.png')
                .add('leaf_small', './magic_forest_leaf_small.png')
                .add('question_icon', './magic_forest_question_icon.png')
                .add('rope', './magic_forest_rope.png')
                .add('rope_small', './magic_forest_rope_small.png')
                .add('scratch_frame', './magic_forest_scratch_frame.png')
                .add('scratch_frame_big', './magic_forest_scratch_frame_big.png')
                .add('shadow_40_percent', './magic_forest_shadow_40_percent.png')
                .add('tent', './magic_forest_tent.png')
                .add('tent_small', './magic_forest_tent_small.png')
                .add('win_up_to_100', './magic_forest_win_up_to_100.png')
                .add('winner', './magic_forest_winner.png')
                .add('winner_frame', './magic_forest_winner_frame.png').load(this.onResoursesLoaded.bind(this));
        });

    }


    buildScene() {
        //----------BACKGROUND--------------
        //adding game background:
        let magicForestSprite = new PIXI.Sprite(PIXI.loader.resources["magic_forest"].texture);
        magicForestSprite.name = "magicForest";
        magicForestSprite.x += -153;
        this.game.app.stage.addChild(magicForestSprite);

        //-------------CHARACTER-----------
        this.initCharacterAnimation('red_idle_loop'); //add character and animate him

        //----------CARDS FIELD------------
        //adding 6 scratch card frames:
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 3; j++) {

                let scratchFrame = new PIXI.Sprite(PIXI.loader.resources["scratch_frame"].texture);

                scratchFrame.name = 'scratchFrame' + i + j;
                scratchFrame.zOrder = 3;
                let x = 73 + (58 + scratchFrame.width) * j;
                let y = 1225 + (58 + scratchFrame.height) * i;
                scratchFrame.x += x;
                scratchFrame.y += y;

                let frame = new PIXI.Sprite(PIXI.loader.resources["frame"].texture);
                frame.name = "frame" + i + j;
                frame.zOrder = 1;

                let scratchContainer = new PIXI.Container();
                scratchContainer.name = 'scratchContainer' + i + j;

                scratchContainer.x += x;
                scratchContainer.y += y;
                scratchContainer.addChild(frame);

                let card = new PIXI.Sprite(PIXI.loader.resources["leaf"].texture); //default card
                card.name = 'card' + i + j;
                card.zOrder = 2;
                scratchContainer.addChild(card);

                this.game.app.stage.addChild(scratchFrame);
                this.game.app.stage.addChild(scratchContainer);
            }
        }

        this.makeCardsScratchable();

        //---------WIN CARD---------
        //adding win card frame:
        let winCardFrame = new PIXI.Sprite(PIXI.loader.resources["winner_frame"].texture);
        winCardFrame.name = 'winCardFrame';
        winCardFrame.zOrder = 1;
        winCardFrame.x = 526;
        winCardFrame.y = 140;
        this.game.app.stage.addChild(winCardFrame);

        let winCardScratchFrame = new PIXI.Sprite(PIXI.loader.resources["scratch_frame_big"].texture);
        winCardScratchFrame.name = 'winCardScratchFrame';
        winCardScratchFrame.interactive = true;
        //winCardScratchFrame.zOrder = 3;
        winCardScratchFrame.x = 615;
        winCardScratchFrame.y = 369;

        //adding a big scratch frame on the winner frame
        let winCardContainer = new PIXI.Container();
        winCardContainer.name = "winCardContainer";
        winCardContainer.x = 615;
        winCardContainer.y = 370;

        //adding background for win card:
        let background = new PIXI.Graphics();
        background.name = "winCardBackground";
        background.beginFill(0xfeeeb8);
        background.drawRoundedRect(0, 0, 365, 365, 50);
        winCardContainer.addChild(background);

        this.game.app.stage.addChild(winCardScratchFrame);
        this.game.app.stage.addChild(winCardContainer);


        this.makeWinCardScratchable();

        //----------UI labels-------------
        //adding frame for text
        let textContainer = new PIXI.Container();
        textContainer.name = "textContainer";
        let textFrame = new PIXI.Sprite(PIXI.loader.resources["frame_for_text"].texture);
        textFrame.name = 'textFrame';
        textFrame.width -= 20;
        textContainer.x = 55;
        textContainer.y = 1043;
        textContainer.addChild(textFrame);
        this.game.app.stage.addChild(textContainer);

        //adding 'win up to $100'
        let winUpTo100 = new PIXI.Sprite(PIXI.loader.resources["win_up_to_100"].texture);
        winUpTo100.name = 'winUpTo100';
        winUpTo100.x = 159;
        winUpTo100.y = 40;
        winUpTo100.zOrder = 1;
        this.game.app.stage.addChild(winUpTo100);

        //----------SHADOW-----------
        //adding 40% shadow on game field:
        let shadow = new PIXI.Sprite(PIXI.loader.resources["shadow_40_percent"].texture);
        shadow.name = "shadow";
        shadow.x = 0;
        shadow.y = 0;
        this.game.app.stage.addChild(shadow);


        //----------BOTTOM FRAME-----------
        //creating container to place all elements of bottom frame
        let container = new PIXI.Container();
        container.name = "bottomContainer";
        container.x = 0;
        container.y = 1530;

        //adding bottom frame with text
        let bottomFrame = new PIXI.Sprite(PIXI.loader.resources["frame2"].texture);
        bottomFrame.name = "bottomFrame";
        //position inside container:
        bottomFrame.x = 0;
        bottomFrame.y = 0;
        container.addChild(bottomFrame);

        //adding start play button
        let button = new PIXI.Sprite(PIXI.loader.resources["button"].texture);
        button.name = "button";
        button.x = 27;
        button.y = 190;
        // Opt-in to interactivity
        button.interactive = true;
        // Shows hand cursor
        button.buttonMode = true;
        // Pointers normalize touch and mouse
        button.on('pointerdown', this.newGame.bind(this));
        container.addChild(button);

        //adding question icon
        let questionIcon = new PIXI.Sprite(PIXI.loader.resources["question_icon"].texture);
        questionIcon.name = "questionIcon";
        questionIcon.x = 330;
        questionIcon.y = 63;
        container.addChild(questionIcon);


        //adding small coin icon
        let coinIcon = new PIXI.Sprite(PIXI.loader.resources["coin_icon_small"].texture);
        coinIcon.name = "coinIcon";
        coinIcon.x = 726;
        coinIcon.y = 252;
        coinIcon.width -= 9;
        coinIcon.height -= 9;
        container.addChild(coinIcon);

        //adding text 'how to play'
        let howToPlayText = new PIXI.Text('How To Play', {
            fontSize: 72,
            fontFamily: 'DRAguSansBlack',
            fill: 0xff8729
        });
        howToPlayText.name = "howToPlayText";
        howToPlayText.x = 440;
        howToPlayText.y = 60;
        container.addChild(howToPlayText);

        //adding button text 'Play for 60'
        let playFor60Text = new PIXI.Text('Play for 60', {
            fontSize: 72,
            fontFamily: 'DRAguSansBlack',
            fill: 0xffffff
        });
        playFor60Text.name = "playFor60Text";
        playFor60Text.x = 371;
        playFor60Text.y = 238;
        container.addChild(playFor60Text);

        this.game.app.stage.addChild(container);

        //----------YOU WIN FRAME---------------
        let gameResultContainer = new PIXI.Container();
        gameResultContainer.name = "gameResultContainer";

        let resultFrame = new PIXI.Sprite(PIXI.loader.resources["frame1"].texture);
        gameResultContainer.addChild(resultFrame);

        gameResultContainer.x = (1097 - gameResultContainer.width) / 2;
        //gameResultContainer.y = 220;
        gameResultContainer.y = -335; //hide out of screen by default
        this.game.app.stage.addChild(gameResultContainer);

        //adding text 'YOU WIN'
        let youWinText = new PIXI.Text('YOU WIN', {
            fontSize: 116,
            fontFamily: 'DRAguSansBlack',
            fill: 0xf45b4e
        });
        youWinText.name = "youWinText";
        youWinText.x = (gameResultContainer.width - youWinText.width) / 2;
        youWinText.y = 30;
        gameResultContainer.addChild(youWinText);

        let payoutText = new PIXI.Text('25', {
            fontSize: 126,
            fontFamily: 'DRAguSansBlack',
            fill: 0x000000
        });
        payoutText.name = "payoutText";
        payoutText.x = 430;
        payoutText.y = 135;
        gameResultContainer.addChild(payoutText);

        let coinIconBig = new PIXI.Sprite(PIXI.loader.resources["coin_icon_big"].texture);
        coinIconBig.name = "coinIconBig";
        coinIconBig.x = 575;
        coinIconBig.y = 160;
        gameResultContainer.addChild(coinIconBig);

    }


    newGame() {
        if (this.isNewGame === true) { //player is going to play his first game in this session
            this.showNewGameScreen();
        }
        else { //player had already played a game in this session and wants to play again
            this.destroyScene();
            this.buildScene();
            this.showNewGameScreen();
        }
    }

    //this method has to be called before the first game in this session
    showPrePlayScreen() {
        this.showShadow();
        //showing bottom container up
        this.game.app.ticker.add(this.moveBottomContainerUp, this);
    }


    //provides displaying of a new game screen
    showNewGameScreen() {
        this.isNewGame = false;
        //removing unnecessary items from scene:
        this.hideShadow();
        this.game.app.ticker.add(this.hideBottomContainerDown, this);
        this.game.app.ticker.add(this.hideWinResultContainerUp, this);

        this.gameStarted = true;

        //getting target card and a set of scratch card names for current game:
        do {
            this.currentGamedData = Game.getScratchCardsSet();
        } while (this.currentGamedData == null);

        this.setCoinsPayout();

        //placing winCard icon under scratch frame
        let winCardContainer = this.getStageChild('winCardContainer');
        let etalonCard = new PIXI.Sprite(PIXI.loader.resources[this.currentGamedData.winCard].texture);
        etalonCard.name = "winCard";
        etalonCard.x = (winCardContainer.width - etalonCard.width) / 2;
        etalonCard.y = (winCardContainer.height - etalonCard.height) / 2;
        etalonCard.zOrder = 2;

        winCardContainer.addChild(etalonCard);
        MainScene.sortContainerChildren(winCardContainer);


        //placing cards under scratch frames
        let cards = this.currentGamedData.cards;
        let counter = 0;
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 3; j++) {
                let scratchContainer = this.getStageChild('scratchContainer' + i + j);
                let cardName = cards[counter];
                let card = MainScene.getContainerChild(scratchContainer, 'card' + i + j);
                card.texture = PIXI.loader.resources[cardName].texture;
                card.x = (scratchContainer.width - card.width) / 2;
                card.y = (scratchContainer.height - card.height) / 2;
                card.name = cards[counter];
                counter++;
            }
        }


        //adding text to describe game rule:
        let textContainer = this.getStageChild("textContainer");
        let topPosition = 28;
        let textPart1 = new PIXI.Text('MATCH THE WINNER', {
            fontSize: 52,
            fontFamily: 'DRAguSansBlack',
            fill: 0xf45b4e
        });
        textPart1.x = 33;
        textPart1.y = topPosition;
        let textPart2 = new PIXI.Text('AND WIN A PRIZE!', {
            fontSize: 52,
            fontFamily: 'DRAguSansBlack',
            fill: 0xf45b4e
        });
        textPart2.x = 575;
        textPart2.y = topPosition;
        let winCardName = this.currentGamedData.winCard + '_small';
        let winCard = new PIXI.Sprite(PIXI.loader.resources[winCardName].texture);
        winCard.x = 478;
        winCard.y = (textContainer.height - winCard.height) / 2;
        textContainer.addChild(textPart1);
        textContainer.addChild(textPart2);
        textContainer.addChild(winCard);
    }

    //this method contains all "scratch" logic of the win card frame
    makeWinCardScratchable() {
        let winCardContainer = this.getStageChild("winCardContainer");
        let renderTexture = PIXI.RenderTexture.create(winCardContainer.width, winCardContainer.height);
        let renderTextureSprite = new PIXI.Sprite(renderTexture);
        renderTextureSprite.x = winCardContainer.x;
        renderTextureSprite.y = winCardContainer.y;
        this.game.app.stage.addChild(renderTextureSprite);
        winCardContainer.mask = renderTextureSprite;
        winCardContainer.interactive = true;
        winCardContainer.on('pointerover', pointerOver.bind(this));
        winCardContainer.on('pointerout', pointerOut.bind(this));
        winCardContainer.on('pointermove', pointerMove.bind(this));

        winCardContainer.on('touchmove', pointerMove.bind(this));
        winCardContainer.on('touchstart', pointerOver.bind(this));
        winCardContainer.on('touchend', pointerOut.bind(this));

        //-----------------EVENT HANDLERS --------------------
        let pointerIsOver = false;
        //prepare circle texture, that will be our brush
        let brush = new PIXI.Graphics();
        brush.beginFill(0xffffff);
        brush.drawCircle(0, 0, 50);
        brush.endFill();

        //min and max cursor points over card to calculate total card square
        //had already been scratched:
        let minX, minY, maxX, maxY;
        //total card square to be scratched:
        let cardSquare = 368 * 368;

        function pointerMove(event) {
            if (this.gameStarted && pointerIsOver) {
                let element = event.currentTarget;
                //checking if current pointer position is a new max or min:
                if (minX === undefined || event.data.global.x < minX)
                    minX = event.data.global.x;
                if (maxX === undefined || event.data.global.x > maxX)
                    maxX = event.data.global.x;
                if (minY === undefined || event.data.global.y < minY)
                    minY = event.data.global.y;
                if (maxY === undefined || event.data.global.y > maxY)
                    maxY = event.data.global.y;
                let scratched = MainScene.checkIfCardScratched(cardSquare, minX, minY, maxX, maxY);
                if (scratched) {
                    //setting up a big brush to scratch all card:
                    brush.width = element.width * 3;
                    brush.height = element.height * 3;
                    let scratchFrame = this.getStageChild("winCardScratchFrame");
                    scratchFrame.visible = false;
                    let winCardBackground = MainScene.getContainerChild(element, "winCardBackground");
                    winCardBackground.visible = false;
                    this.cardsScratched++;
                    //removing all events listeners:
                    element.removeAllListeners();
                    this.showAnimation("happy_bonus");

                    if (this.cardsScratched === 7) //all cards were scratched
                        this.gameOver();
                }
                brush.position.copy(event.data.global);
                brush.position.x -= element.x;
                brush.position.y -= element.y;
                this.game.app.renderer.render(brush, renderTexture, false, null, false);
            }
        }

        function pointerOver(event) {
            if (this.gameStarted) {
                let element = event.currentTarget;
                if (element !== null
                    && this.charState !== "worry") {
                    this.charState = "worry";
                    this.character.state.setAnimation(0, "red_worry_st", false);
                    this.character.state.addAnimation(0, "red_worry_loop", true, 0);
                }
                pointerIsOver = true;
            }
        }

        function pointerOut(event) {
            if (this.gameStarted && this.charState === "worry") {
                this.charState = "idle";
                this.character.state.setAnimation(0, "red_worry_end", false);
                this.character.state.addAnimation(0, "red_idle_loop", true, 0);
            }
            pointerIsOver = false;
        }
    }

    //this method contains all "scratch" logic of every of 6 cards frames
    makeCardsScratchable() {
        let stage = this.game.app.stage;
        let renderer = this.game.app.renderer;

        for (let i = 0; i < 2; i++)
            for (let j = 0; j < 3; j++) {
                let scratchContainer = this.getStageChild('scratchContainer' + i + j);
                let renderTexture = PIXI.RenderTexture.create(scratchContainer.width, scratchContainer.height);
                let renderTextureSprite = new PIXI.Sprite(renderTexture);
                renderTextureSprite.x = scratchContainer.x;
                renderTextureSprite.y = scratchContainer.y;
                stage.addChild(renderTextureSprite);

                scratchContainer.mask = renderTextureSprite;

                scratchContainer.interactive = true;
                scratchContainer.on('pointerover', pointerOver.bind(this));
                scratchContainer.on('pointerout', pointerOut.bind(this));
                scratchContainer.on('pointermove', pointerMove.bind(this));

                scratchContainer.on('touchmove', pointerMove.bind(this));
                scratchContainer.on('touchstart', pointerOver.bind(this));
                scratchContainer.on('touchend', pointerOut.bind(this));

                //-----------------EVENT HANDLERS --------------------
                let pointerIsOver = false;
                //prepare circle texture, that will be our brush
                let brush = new PIXI.Graphics();
                brush.beginFill(0xffffff);
                brush.drawCircle(0, 0, 35);
                brush.endFill();

                //min and max cursor points over card to calculate total card square
                //had already been scratched:
                let minX, minY, maxX, maxY;
                //total card square to be scratched:
                let cardSquare = scratchContainer.width * scratchContainer.height;

                function pointerMove(event) {
                    if (pointerIsOver && this.gameStarted) {
                        let element = event.currentTarget;

                        //checking if current pointer position is a new max or min:
                        if (minX === undefined || event.data.global.x < minX)
                            minX = event.data.global.x;
                        if (maxX === undefined || event.data.global.x > maxX)
                            maxX = event.data.global.x;
                        if (minY === undefined || event.data.global.y < minY)
                            minY = event.data.global.y;
                        if (maxY === undefined || event.data.global.y > maxY)
                            maxY = event.data.global.y;
                        let scratched = MainScene.checkIfCardScratched(cardSquare, minX, minY, maxX, maxY);
                        if (scratched) {
                            //setting up a big brush to scratch all card:
                            brush.width = element.width * 3;
                            brush.height = element.height * 3;
                            this.cardsScratched++;
                            //removing all events listeners:
                            element.removeAllListeners();

                            //checking if scratched card is a win card:
                            let winCardName = this.currentGamedData.winCard;
                            let cardScratched = MainScene.getContainerChild(element, winCardName);
                            if (cardScratched !== undefined) {
                                this.showAnimation("happy_card");
                            }
                            else {
                                this.showAnimation("disappointed");
                            }
                            if (this.cardsScratched === 7)
                                this.gameOver();
                        }
                        brush.position.copy(event.data.global);
                        brush.position.x -= element.x;
                        brush.position.y -= element.y;
                        renderer.render(brush, renderTexture, false, null, false);
                    }
                }

                function pointerOver(event) {
                    if (this.gameStarted) {
                        let element = event.currentTarget;
                        if (element !== null
                            && this.charState !== "worry") {
                            this.charState = "worry";
                            this.character.state.setAnimation(0, "red_worry_st", false);
                            this.character.state.addAnimation(0, "red_worry_loop", true, 0);
                        }
                        pointerIsOver = true;
                    }
                }

                function pointerOut(event) {
                    if (this.gameStarted && this.charState === "worry") {
                        this.charState = "idle";
                        this.character.state.setAnimation(0, "red_worry_end", false);
                        this.character.state.addAnimation(0, "red_idle_loop", true, 0);
                    }
                    pointerIsOver = false;
                }
            }
    }


    gameOver() {
        this.gameStarted = false;
        this.cardsScratched = 0;
        this.showShadow();
        //showing bottom container up
        this.game.app.ticker.add(this.moveBottomContainerUp, this);
        //showing game result container:
        this.game.app.ticker.add(this.showWinResultContainerDown, this);

    }

    //-------------------------HELPERS-------------------------

    getStageChild(name) {
        return this.game.app.stage.children.find((sprite) => {
            return sprite.name === name;
        });
    }

    static getContainerChild(container, childName) {
        return container.children.find((sprite) => {
            return sprite.name === childName;
        });
    }

    static sortContainerChildren(container) {
        container.children.sort((a, b) => {
            return a.zOrder - b.zOrder;
        });
    }

    showShadow() {
        let shadow = this.getStageChild("shadow");
        shadow.visible = true;
    }

    hideShadow() {
        let shadow = this.getStageChild("shadow");
        shadow.visible = false;

    }

    hideBottomContainerDown() {
        this.tickerCounter++; //counting ticks
        let bottomContainer = this.getStageChild('bottomContainer');
        if (bottomContainer.y < 1920) //container is still not on his place
        {
            bottomContainer.y += 10;
        }
        else //container has arrived on his place
        {
            this.game.app.ticker.remove(this.hideBottomContainerDown, this);
            this.tickerCounter = 0;
        }
    }

    moveBottomContainerUp() {
        this.tickerCounter++; //counting ticks
        let bottomContainer = this.getStageChild('bottomContainer');
        if (bottomContainer.y > 1535) //container is still not on his place
        {
            bottomContainer.y -= 10;
        }
        else //container has arrived on its place
        {
            this.game.app.ticker.remove(this.moveBottomContainerUp, this);
            this.tickerCounter = 0;
        }
    }

    hideWinResultContainerUp() {
        this.tickerCounter++; //counting ticks
        let gameResultContainer = this.getStageChild("gameResultContainer");
        if (gameResultContainer.y > -335) //container is still not on his place
        {
            gameResultContainer.y -= 15;
        }
        else //container has arrived on his place
        {
            this.game.app.ticker.remove(this.hideWinResultContainerUp, this);
            this.tickerCounter = 0;
        }
    }

    showWinResultContainerDown() {
        this.tickerCounter++; //counting ticks
        let gameResultContainer = this.getStageChild("gameResultContainer");
        if (gameResultContainer.y < 220) //container is still not on his place
        {
            gameResultContainer.y += 15;
        }
        else //container has arrived on its place
        {
            this.game.app.ticker.remove(this.showWinResultContainerDown, this);
            this.tickerCounter = 0;
        }
    }

    //shows 1 dollar payout (if such payout was defined for this game)
    setCoinsPayout() {
        if(this.currentGamedData.win)
        {
            let gameResultContainer = this.getStageChild("gameResultContainer");
            let payoutText = MainScene.getContainerChild(gameResultContainer, "payoutText");
            payoutText.text = this.currentGamedData.bonus;
            if (this.currentGamedData.bonus >= 100)
                payoutText.x -= 50;
        }
    }


    initCharacterAnimation(animationName) {
        this.character = this.getStageChild('character');
        if (this.character === undefined) //not found - character does not exist yet
        {
            this.character = new PIXI.spine.Spine(PIXI.loader.resources["character"].spineData);
            this.character.name = 'character';
            this.character.x = 310;
            this.character.y = 620;
            this.character.skeleton.setToSetupPose();
            this.character.update(0);
            this.character.autoUpdate = true;
            this.game.app.stage.addChild(this.character);
        }
        //starting animation loop:
        if (this.character.state.hasAnimation(animationName)) {
            this.character.skeleton.setToSetupPose();
            this.character.update(0);
            this.character.state.setAnimation(0, animationName, true);
        }
    }

    showAnimation(name) {
        this.charState = name;
        //show full animation once:
        this.character.state.setAnimation(0, 'red_' + name + '_st', false);
        this.character.state.addAnimation(0, 'red_' + name + '_loop', false, 0);
        this.character.state.addAnimation(0, 'red_' + name + '_end', false, 0);
        //set default animation loop:
        this.character.state.addAnimation(0, 'red_idle_loop', true, 0);
        this.charState = 'idle';
    }

    static checkIfCardScratched(elementSquare, minX, minY, maxX, maxY) {
        let scratched = false;
        let squareScratched = (maxX - minX) * (maxY - minY);
        if (squareScratched >= elementSquare * 0.6) //60% of card already scratched
            scratched = true;
        return scratched;
    }

    destroyScene() {
        this.game.app.stage.children = [];
    }

}