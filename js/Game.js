import MainScene from "./MainScene";
export default class Game {
    constructor() {
        this.app = new PIXI.Application({width: 1097, height: 1920});
    }

    init() {
        document.body.appendChild(this.app.view);
        this.setActiveScene();
    }

    //this method will be useful in case of multistage game (future extension)
    setActiveScene(scene) {
        if (scene == null)   //initializing default scene
            this.activeScene = new MainScene(this);
        else
            this.activeScene = scene;
    }


    static getScratchCardsSet() {
        let scratchCards = ['bonfire', 'bow', 'leaf', 'rope', 'tent'];
        let bonusSum = 25; //bonus guaranteed
        let win = true; //flag to indicate if three-card combination exists
        let randomForCardsCombination = Math.random() * (100 - 1) + 1;
        let cardForCombination = null; //no three-card combination 70% probability
        if(randomForCardsCombination <= 10) //bonfire - 10% probability
        {
            cardForCombination = scratchCards[0];
            bonusSum += 25;
        }
        else if(randomForCardsCombination <= 18)//bow - 8% probability
        {
            cardForCombination = scratchCards[1];
            bonusSum +=30;
        }
        else if(randomForCardsCombination <= 24){ //leaf - 6% probability
            cardForCombination = scratchCards[2];
            bonusSum += 35;
        }
        else if(randomForCardsCombination <= 28){  //rope - 4% probability
            cardForCombination = scratchCards[3];
            bonusSum += 50;
        }
        else if(randomForCardsCombination <= 30) //tent - 2% probability
        {
            cardForCombination = scratchCards[4];
            bonusSum += 100;
        }
        else
        {
            win = false;
        }
        //creating final cards set for game
        let cardsSet = [];

        if (win) //three-card combination exists
        {
            //adding three win cards
            cardsSet.push(cardForCombination);
            cardsSet.push(cardForCombination);
            cardsSet.push(cardForCombination);
            let winCardIndex = scratchCards.indexOf(cardForCombination);
            //generating indexes of three other cards:
            for (let i = 0; i < 3; i++) {
                let index = -1;
                //avoiding generating win card index:
                do {
                    index = Math.floor(Math.random() * 4);
                } while (index === winCardIndex);
                cardsSet.push(scratchCards[index]);
            }
        }
        else //no three-card combination
        {
            //generating pseudo win card
            let index = Math.floor(Math.random() * 4);
            cardForCombination = scratchCards[index];
            //creating array of 8 elements, that contains every card only twice (to avoid creating win combination):
            let looseArray = [];
            scratchCards.forEach((value) => {
                looseArray.push(value);
                looseArray.push(value);
            });
            //shuffling this array randomly:
            looseArray.forEach((value, index) => {
                let newIndex = Math.floor(Math.random() * 7);
                let tmp = looseArray[index];
                looseArray[index] = looseArray[newIndex];
                looseArray[newIndex] = tmp;
            });
            cardsSet = looseArray.slice(0, 6); //we need only first 6 cards
        }
        //shuffling this array randomly:
        cardsSet.forEach((value, index) => {
            let newIndex = Math.floor(Math.random() * 5);
            let tmp = cardsSet[index];
            cardsSet[index] = cardsSet[newIndex];
            cardsSet[newIndex] = tmp;
        });
        return {win: win, winCard: cardForCombination, cards: cardsSet, bonus: bonusSum};
    }

}